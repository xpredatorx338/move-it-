﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{
    //Load main level
    public void GoToGameScene()
    {
        SceneManager.LoadScene("Level 1");
        //restores time
        Time.timeScale = 1;
    }
    //This will close the game
    public void CloseGame()
    {
        Application.Quit();
    }
    //will open twitter link
    public void GoToTwitter()
    {
        Application.OpenURL("https://twitter.com/_xycy_");
    }
    //will open instagram link
    public void GoToInstagram()
    {
        Application.OpenURL("https://www.instagram.com/_xycy_/");
    }

}
