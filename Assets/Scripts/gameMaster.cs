﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameMaster : MonoBehaviour
{

    public GameObject restartPanel;

    public TMP_Text timerDisplay;
    public GameObject pauseButton;
    private bool asLost;

    public float timer;

    //Timer
    private void Update()
    {
        if (asLost == false)
        {
            //adds time to the timer 
            timerDisplay.text = timer.ToString("F0");
        }

        if (timer <= 0)
        {
            //takes you to the new level 
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            //restores time
            Time.timeScale = 1;
        }
        else
        {
            //removes time from timer
            timer -= Time.deltaTime;
        }
    }
    //Restart panel delay
    public void GameOver()
    {
        //turns on function
        asLost = true;
        //delays panel 2 secs
        Invoke("Delay", 1.5f);

    }
    //Active Screens
    void Delay()
    {
        //restart panel on
        restartPanel.SetActive(true);
        //pause button off
        pauseButton.SetActive(false);
        //Stops time
        Time.timeScale = 0;
    }
    //loads the menu
    public void goToMainMenu()
    {

        SceneManager.LoadScene("Menu");
        //restores time
        Time.timeScale = 1;
    }
    //restarts the game
    public void Restart()
    {
        //restarts the current level 
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //restores time
        Time.timeScale = 1;
    }
}
