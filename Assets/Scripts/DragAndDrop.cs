﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DragAndDrop : MonoBehaviour
{
    bool moveallowed;
    Collider2D col;

    public GameObject touchParticles;
    public GameObject deadParticle;


    private gameMaster gm;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<gameMaster>();
        col = GetComponent<Collider2D>();
    }


    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);

            if (touch.phase == TouchPhase.Began)
            {
                Collider2D touchCollider = Physics2D.OverlapPoint(touchPosition);
                if (col == touchCollider)
                {
                    //Adds touch particles
                    Instantiate(touchParticles, transform.position, Quaternion.identity);
                    //Allows to move
                    moveallowed = true;
                    //adds touch sounds
                    FindObjectOfType<AudioManager>().PlaySound("Pop");
                }
            }

            if (touch.phase == TouchPhase.Moved)
            {
                if (moveallowed)
                {
                    //Drags the enemy when touched
                    transform.position = new Vector2(touchPosition.x, touchPosition.y);
                }
            }

            if (touch.phase == TouchPhase.Ended)
            {
                //Stops touch in other objects
                moveallowed = false;
            }
        }

    }
    private void OnTriggerEnter2D(Collider2D Collision)
    {
        if (Collision.tag == "Enemy")
        {
            //Adds the death particles
            Instantiate(deadParticle, transform.position, Quaternion.identity);
            //Game is ended 
            gm.GameOver();
            //Enemy gets destroy on collion 
            Destroy(gameObject);
            //will add death sound when collions have happen
            FindObjectOfType<AudioManager>().PlaySound("Death");
        }

        if (Collision.tag == "Player")
        {
            //add destroy particles for fun 
            Instantiate(deadParticle, transform.position, Quaternion.identity);
            //Destroys the player
            Destroy(gameObject);
        }
    }
}
