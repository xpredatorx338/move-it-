﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseMenu : MonoBehaviour
{
    public GameObject pauseMenuPanel, pauseButton;

    //pauses the game
    public void Pause()
    {
        //Stops time
        Time.timeScale = 0;
        //pause button is off
        pauseButton.SetActive(false);
        //pause panel is on 
        pauseMenuPanel.SetActive(true);
    }
    //unpauses the game
    public void unPaused()
    {
        //continues time
        Time.timeScale = 1;
        //pause panel is off
        pauseMenuPanel.SetActive(false);
        //pause button is on
        pauseButton.SetActive(true);
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
        //restores time
        Time.timeScale = 1;
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
