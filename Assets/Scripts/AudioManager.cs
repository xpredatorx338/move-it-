﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

[System.Serializable]
public class Sound
{

    private AudioSource source;

    public AudioMixerGroup audioMixerGroup;
    public string clipName;
    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume;
    [Range(0f, 3f)]
    public float pitch;

    public bool loop = false;
    public bool PlayonAwake = false;

    public void SetSource(AudioSource _source)
    {

        source = _source;
        source.clip = clip;
        source.pitch = pitch;
        source.volume = volume;
        source.playOnAwake = PlayonAwake;
        source.loop = loop;
        source.outputAudioMixerGroup = audioMixerGroup;
    }

    public void Play()
    {
        source.Play();
    }
}


public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [SerializeField]
    Sound[] sound;

    public Sound[] Sound { get => sound; set => sound = value; }

    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        for (int i = 0; i < Sound.Length; i++)
        {
            GameObject _go = new GameObject("sound_" + i + "_" + Sound[i].clipName);
            _go.transform.SetParent(this.transform);
            Sound[i].SetSource(_go.AddComponent<AudioSource>());

        }
        // this are the audio files that are used in the audio manager
        PlaySound("Cosmic Tribe Tribal Ambient");
        PlaySound("Midnight Ambient");

    }

    public void PlaySound(string _name)
    {
        for (int i = 0; i < Sound.Length; i++)
        {
            if (Sound[i].clipName == _name)
            {
                Sound[i].Play();
                return;
            }
        }
    }
}